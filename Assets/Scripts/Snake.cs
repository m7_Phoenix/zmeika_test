﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Snake : MonoBehaviour
{
  public List<Transform> tails;
  public float bonesDistance;
  public GameObject bonePrefabs;
  private Transform _transform;
  public float speed = 3.0f;
  public float obstacleRange = 5.0f;


  void Start()
  {
    _transform = GetComponent<Transform>();
  }

  void Update()
  {
    MoveSnake(_transform.position + _transform.forward * speed);
    transform.Translate(0, 0, speed * Time.deltaTime);
    Ray ray = new Ray(transform.position, transform.forward);
    RaycastHit hit;

    if (Physics.SphereCast(ray, 0.75f, out hit))
    {
      if (hit.transform.GetComponent<BoneSimpleScript>())
      {
        return;
      }
      if (hit.transform.GetComponent<SimpleScipt>())
      {
        return;
      }
      if (hit.distance < obstacleRange)
      {
        float angle = UnityEngine.Random.Range(-110, 110);
        transform.Rotate(0, angle, 0);
      }
    }
  }

  void MoveSnake(Vector3 newPosition)
  {
    float sqrDistance = bonesDistance * bonesDistance;
    Vector3 previousPosition = _transform.position;

    foreach (var bone in tails)
    {
      if ((bone.position - previousPosition).sqrMagnitude > sqrDistance)
      {
        var temp = bone.position;
        bone.position = previousPosition;
        previousPosition = temp;
      }
      else
      {
        break;
      }
    }
    _transform.position = newPosition;
  }
  private void OnCollisionEnter(Collision collision)
  {
    if (collision.gameObject.tag == "Food")
    {
      Destroy(collision.gameObject);
      var bone = Instantiate(bonePrefabs);
      tails.Add(bone.transform);
    }
  }
  }
