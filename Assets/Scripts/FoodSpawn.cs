﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawn : MonoBehaviour
{
    [SerializeField] GameObject spawnArea = null;
    [SerializeField] GameObject food = null;
    [SerializeField] float spawnPeriod;
   
   
    float timer = 0;
    private void Update()
    {
        Spawn();
    }
    public void Spawn()
    {
        timer += Time.deltaTime;
            if (timer >= Random.Range(spawnPeriod, spawnPeriod))
            {
             Instantiate(food, RandomPointInBox(spawnArea.transform.position, spawnArea.transform.lossyScale), transform.rotation, spawnArea.transform);
                timer = 0;
            }
        }
    Vector3 RandomPointInBox(Vector3 center, Vector3 size)
    {
        return center + new Vector3(
                   (Random.value - 0.5f) * size.x,
                   (Random.value - 0.0f) * size.y,
                   (Random.value - 0.5f) * size.z);
    }
    
}
