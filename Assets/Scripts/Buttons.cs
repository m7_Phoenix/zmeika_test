﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
   [SerializeField] GameObject allObjects;
   
   public void RestartGame()
   {
      
      Application.LoadLevel(Application.loadedLevel);
      
   }

   public void StartGame()
   {
       SceneManager.LoadSceneAsync(1);
   }
}
